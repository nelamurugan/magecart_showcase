<?php

class Magecart_Constants
{
    const SC_PHOTOS_DIR ='media/showcase/photos/';
    public static $SC_STATUS = array("0" => 'Pending Review', "1" => 'Approved', "2" =>'Approval Denied', "3" =>'User Disabled');
}