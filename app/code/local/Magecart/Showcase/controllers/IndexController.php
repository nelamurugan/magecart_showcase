<?php

class Magecart_Showcase_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {

        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Kurinjie Product Photos Showcase by Our customers :)"));
        $breadcrumbs = $this->getLayout()->getBlock("breadcrumbs");
        $breadcrumbs->addCrumb("home", array(
            "label" => $this->__("Home Page"),
            "title" => $this->__("Home Page"),
            "link" => Mage::getBaseUrl()
        ));

        $breadcrumbs->addCrumb("my photos", array(
            "label" => $this->__("Product Photos"),
            "title" => $this->__("Product Photos")
        ));

        $this->renderLayout();
    }

    public function submitAction()
    {
        $post_data = $this->getRequest()->getPost();
        if ($post_data && count($post_data)) {
            try {
                $helper = Mage::helper('showcase');

                if (@$_FILES['photo_file']['name'] != '') {
                    $path = BP . DS. Magecart_Constants::SC_PHOTOS_DIR ;
                    $varienFile = new Varien_Io_File();
                    $obj = $varienFile->mkdir($path);
                    $uploader = new Varien_File_Uploader('photo_file');
                    $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'png', 'gif'));
                    $uploader->setAllowRenameFiles(true);
                    $uploader->setFilesDispersion(false);
                    $destFile = $path . $helper->stringtoUrlKey($_FILES['photo_file']['name']);
                    $filename = $uploader->getNewFileName($destFile);
                    $uploader->save($path, $filename);
                    $filename = $uploader->getUploadedFileName();
                    $post_data['photo_file'] = $filename;
                }
                $customer = Mage::getSingleton('customer/session')->getCustomer();
                $post_data['customer_id'] =  0;
                $post_data['status'] =  0;
                if($customer && $customer->getId()){
                    $post_data['customer_id'] = $customer->getId();
                }

                Mage::getModel("showcase/photos")
                    ->addData($post_data)
                    ->save();
                Mage::getSingleton("core/session")->addSuccess('Your Photo Submitted For Verification Successfully.');
                $this->_redirectUrl(Mage::getUrl()."showcase");
                return;
            } catch (Exception $e) {
                Mage::getSingleton("core/session")->addError($e->getMessage());
                $this->_redirectUrl(Mage::getUrl()."showcase");
                return;
            }
        }
        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("Submit Photo"));
        $this->renderLayout();
    }

    public function myphotosAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock("head")->setTitle($this->__("My Photos"));
        $this->renderLayout();
    }
}