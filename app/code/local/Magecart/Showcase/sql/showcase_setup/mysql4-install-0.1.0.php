<?php
$installer = $this;
$installer->startSetup();
$sql = <<<SQLTEXT
CREATE TABLE `mc_showcase_photos` (
  `photo_id` int(12) NOT NULL,
  `title` varchar(2550) DEFAULT NULL,
  `description` text,
  `photo_file` varchar(2550) DEFAULT NULL,
  `status` int(12) DEFAULT '0',
  `added_on` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `customer_id` int(12) DEFAULT NULL,
  `category_id` int(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Customer Product Photo Showcase';


ALTER TABLE `mc_showcase_photos` ADD PRIMARY KEY (`photo_id`);


ALTER TABLE `mc_showcase_photos` MODIFY `photo_id` int(12) NOT NULL AUTO_INCREMENT;
SQLTEXT;

$installer->run($sql);

$installer->endSetup();
	 