<?php

class Magecart_Showcase_Block_Adminhtml_Photos_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId("photos_tabs");
        $this->setDestElementId("edit_form");
        $this->setTitle(Mage::helper("showcase")->__("Item Information"));
    }

    protected function _beforeToHtml()
    {
        $this->addTab("form_section", array(
            "label" => Mage::helper("showcase")->__("Item Information"),
            "title" => Mage::helper("showcase")->__("Item Information"),
            "content" => $this->getLayout()->createBlock("showcase/adminhtml_photos_edit_tab_form")->toHtml(),
        ));
        return parent::_beforeToHtml();
    }

}
