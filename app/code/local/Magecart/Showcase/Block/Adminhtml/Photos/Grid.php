<?php

class Magecart_Showcase_Block_Adminhtml_Photos_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId("photosGrid");
        $this->setDefaultSort("photo_id");
        $this->setDefaultDir("DESC");
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel("showcase/photos")->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn("photo_id", array(
            "header" => Mage::helper("showcase")->__("ID"),
            "align" => "right",
            "width" => "50px",
            "type" => "number",
            "index" => "photo_id",
        ));

        $this->addColumn("title", array(
            "header" => Mage::helper("showcase")->__("Title"),
            "index" => "title",
        ));
        $this->addColumn("description", array(
            "header" => Mage::helper("showcase")->__("Description"),
            "index" => "description",
        ));
        $this->addColumn("photo_file", array(
            "header" => Mage::helper("showcase")->__("Photo"),
            "index" => "photo_file",
            'renderer' => new Magecart_Showcase_Block_Adminhtml_Photos_Renderer_Photo(),
        ));
        $this->addColumn('status', array(
            'header' => Mage::helper('showcase')->__('Status'),
            'index' => 'status',
            'type' => 'options',
            'options' => Mage::getModel('showcase/source_status')->getOptionArray(),
        ));

        $this->addColumn('added_on', array(
            'header' => Mage::helper('showcase')->__('Submitted On'),
            'index' => 'added_on',
            'type' => 'datetime',
        ));
        $this->addColumn("customer_id", array(
            "header" => Mage::helper("showcase")->__("Customer"),
            "index" => "customer_id",
            'renderer' => new Magecart_Showcase_Block_Adminhtml_Photos_Renderer_Customer(),
        ));
        $this->addColumn("category_id", array(
            "header" => Mage::helper("showcase")->__("Category"),
            "index" => "category_id",
            'renderer' => new Magecart_Showcase_Block_Adminhtml_Photos_Renderer_Category(),
        ));
        $this->addExportType('*/*/exportCsv', Mage::helper('sales')->__('CSV'));
        $this->addExportType('*/*/exportExcel', Mage::helper('sales')->__('Excel'));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {
        return false;//$this->getUrl("*/*/edit", array("id" => $row->getId()));
    }


    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('photo_id');
        $this->getMassactionBlock()->setFormFieldName('photo_ids');
        $this->getMassactionBlock()->setUseSelectAll(true);
        $this->getMassactionBlock()->addItem('remove_photos', array(
            'label' => Mage::helper('showcase')->__('Remove Photos'),
            'url' => $this->getUrl('*/adminhtml_photos/massRemove'),
            'confirm' => Mage::helper('showcase')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('approve_all', array(
            'label' => Mage::helper('showcase')->__('Approve Selected'),
            'url' => $this->getUrl('*/adminhtml_photos/massStatus/status/1'),
            'confirm' => Mage::helper('showcase')->__('Are you sure?')
        ));

        $this->getMassactionBlock()->addItem('dis_approve_all', array(
            'label' => Mage::helper('showcase')->__('Disable Selected'),
            'url' => $this->getUrl('*/adminhtml_photos/massStatus/status/2'),
            'confirm' => Mage::helper('showcase')->__('Are you sure?')
        ));

        return $this;
    }
}