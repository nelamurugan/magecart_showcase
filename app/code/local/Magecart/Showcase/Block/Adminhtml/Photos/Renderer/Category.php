<?php

class Magecart_Showcase_Block_Adminhtml_Photos_Renderer_Category extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $value = $row->getData();
        return Mage::getModel('catalog/category')->load($value['category_id'])->getName();
    }

}