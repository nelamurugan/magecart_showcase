<?php

class Magecart_Showcase_Block_Adminhtml_Photos_Renderer_Photo extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $baseUrl = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_WEB);
        $url = $baseUrl . Magecart_Constants::SC_PHOTOS_DIR . $row->getData('photo_file');
        return '<a href="'.$url.'" target="_blank"><img src="'.$url.'" width="100" height="100"/></a>';
    }

}