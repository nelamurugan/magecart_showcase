<?php

class Magecart_Showcase_Block_Adminhtml_Photos_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {

        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset("showcase_form", array("legend" => Mage::helper("showcase")->__("Item information")));


        $fieldset->addField("photo_id", "text", array(
            "label" => Mage::helper("showcase")->__("ID"),
            "name" => "photo_id",
        ));

        $fieldset->addField("title", "text", array(
            "label" => Mage::helper("showcase")->__("Title"),
            "name" => "title",
        ));

        $fieldset->addField("description", "text", array(
            "label" => Mage::helper("showcase")->__("Description"),
            "name" => "description",
        ));

        $fieldset->addField("photo_file", "text", array(
            "label" => Mage::helper("showcase")->__("Photo"),
            "name" => "photo_file",
        ));

        $fieldset->addField('status', 'select', array(
            'label' => Mage::helper('showcase')->__('Status'),
            'values' => Mage::getModel('showcase/source_status')->getAllOptions(),
            'name' => 'status',
        ));
        $dateFormatIso = Mage::app()->getLocale()->getDateTimeFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );

        $fieldset->addField('added_on', 'date', array(
            'label' => Mage::helper('showcase')->__('Submitted On'),
            'name' => 'added_on',
            'time' => true,
            'image' => $this->getSkinUrl('images/grid-cal.gif'),
            'format' => $dateFormatIso
        ));
        $fieldset->addField("customer_id", "text", array(
            "label" => Mage::helper("showcase")->__("Customer"),
            "name" => "customer_id",
        ));

        $fieldset->addField("category_id", "text", array(
            "label" => Mage::helper("showcase")->__("Category"),
            "name" => "category_id",
        ));


        if (Mage::getSingleton("adminhtml/session")->getPhotosData()) {
            $form->setValues(Mage::getSingleton("adminhtml/session")->getPhotosData());
            Mage::getSingleton("adminhtml/session")->setPhotosData(null);
        } elseif (Mage::registry("photos_data")) {
            $form->setValues(Mage::registry("photos_data")->getData());
        }
        return parent::_prepareForm();
    }
}
