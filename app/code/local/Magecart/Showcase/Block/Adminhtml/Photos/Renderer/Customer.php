<?php

class Magecart_Showcase_Block_Adminhtml_Photos_Renderer_Customer extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $value = $row->getData();
        $customer = Mage::getModel('customer/customer')->load($value['customer_id']);
        if($customer && $customer->getId())
        return $customer->getFirstname();
        return 'Guest';
    }

}