<?php


class Magecart_Showcase_Block_Adminhtml_Photos extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {

        $this->_controller = "adminhtml_photos";
        $this->_blockGroup = "showcase";
        $this->_headerText = Mage::helper("showcase")->__("Photos Manager");
        $this->_addButtonLabel = Mage::helper("showcase")->__("Add New Item");
        parent::__construct();
        $this->_removeButton('add');
    }

}