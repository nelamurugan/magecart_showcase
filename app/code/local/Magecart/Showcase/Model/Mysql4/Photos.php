<?php

class Magecart_Showcase_Model_Mysql4_Photos extends Mage_Core_Model_Mysql4_Abstract
{
    protected function _construct()
    {
        $this->_init("showcase/photos", "photo_id");
    }
}