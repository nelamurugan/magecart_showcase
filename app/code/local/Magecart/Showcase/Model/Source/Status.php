<?php

class Magecart_Showcase_Model_Source_Status extends Mage_Eav_Model_Entity_Attribute_Source_Abstract
{
    public function getAllOptions()
    {
        if (is_null($this->_options)) {
            $statusTypes = Magecart_Constants::$SC_STATUS;
            foreach ($statusTypes as $key => $type) {
                $this->_options[] = array(
                    'value' => $key,
                    'label' => $type,
                );
            }
        }
        return $this->_options;
    }

    public function getSelectFieldOptions()
    {
        $this->_options = $this->getAllOptions();
        return $this->_options;
    }

    /**
     * Retrieve option array
     *
     * @return array
     */
    public function getOptionArray($type_id = false, $category_id = false)
    {
        $_options = array();
        foreach ($this->getAllOptions($type_id, $category_id) as $option) {
            if ($option["value"] != "") {
                $_options[$option["value"]] = $option["label"];
            }
        }
        return $_options;
    }

    /**
     * Get a text for option value
     *
     * @param string|integer $value
     * @return string
     */
    public function getOptionText($value)
    {
        $options = $this->getAllOptions();
        foreach ($options as $option) {
            if ($option["value"] == $value) {
                return $option["label"];
            }
        }
        return false;
    }

}