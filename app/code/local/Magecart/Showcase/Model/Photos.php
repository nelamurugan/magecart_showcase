<?php

class Magecart_Showcase_Model_Photos extends Mage_Core_Model_Abstract
{
    protected function _construct()
    {
        $this->_init("showcase/photos");
    }

    public function getUserPhotos()
    {
        $userPhotosCollections = Mage::getModel('showcase/photos')->getCollection();
//        $userPhotosCollections->addFieldToFilter('status',1);
        $customer = Mage::getSingleton('customer/session')->getCustomer();
        if($customer && $customer->getId()){
            $userPhotosCollections->addFieldToFilter('customer_id',$customer->getId());
        }
        $userPhotosCollections->getSelect()->order('photo_id desc');
        return $userPhotosCollections;
    }

    public function getCategoryPhotos()
    {
        $userPhotosCollections = Mage::getModel('showcase/photos')->getCollection();
        $userPhotosCollections->addFieldToFilter('status',1);
        $category = Mage::registry('current_category');
        if($category){
            $userPhotosCollections->addFieldToFilter('category_id',$category->getId());
        }
        $userPhotosCollections->getSelect()->order('RAND()');
        return $userPhotosCollections;
    }

    public function getCurrentViewPhotos()
    {
        $userPhotosCollections = Mage::getModel('showcase/photos')->getCollection();
        $userPhotosCollections->addFieldToFilter('status',1);
        $userPhotosCollections->getSelect()->order('photo_id desc');
        return $userPhotosCollections;
    }

}
	 