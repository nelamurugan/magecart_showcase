<?php

class Magecart_Showcase_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getProductCategories()
    {
//        $api = Mage::getModel('catalog/category_api');
//        $childrens = $api->tree(2);

        $categories = Mage::getModel('catalog/category')
            ->getCollection()
            ->addFieldToFilter('is_active', 1)
            ->addAttributeToSelect('*');
        return $categories;
    }

    public function getPhotoSubmitUrl()
    {
        return Mage::getUrl()."showcase/index/submit/";
    }

    public function getPhotoShowcaseUrl()
    {
        return Mage::getUrl().'showcase';
    }

    public function stringtoUrlKey($orgString, $separator = '-', $extensionNeeded = true)
    {
        $accents_regex = '~&([a-z]{1,2})(?:acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i';
        $special_cases = array('&' => 'and');
        // $string = mb_strtolower(trim($string), 'UTF-8');
        $string = $orgString = strtolower(trim($orgString));
        $extension = $this->getFileExtension($orgString);
        $string = str_ireplace('.' . $extension, '', $orgString);
        $string = str_replace(array_keys($special_cases), array_values($special_cases), $string);
        $string = preg_replace($accents_regex, '$1', htmlentities($string, ENT_QUOTES, 'UTF-8'));
        $string = preg_replace("/[^a-z0-9]/u", "$separator", $string);
        $string = preg_replace("/[$separator]+/u", "$separator", $string);
        $string = trim($string, "-");
        if ($extensionNeeded) {
            $string = $string . "." . $extension;
        }
        return $string;
    }


    public function getFileExtension($file_name)
    {
        return substr(strrchr($file_name, '.'), 1);
    }
}
	 